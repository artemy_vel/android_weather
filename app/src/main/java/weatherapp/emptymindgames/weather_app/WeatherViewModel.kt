package weatherapp.emptymindgames.weather_app

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import weatherapp.emptymindgames.weather_app.data.AppDatabase
import weatherapp.emptymindgames.weather_app.data.CityFound

class WeatherViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: WeatherRepository


    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(
        viewModelJob + Dispatchers.Main
    )



    val cities = MutableLiveData<List<CityFound>>()


    init {
        val cityDao = AppDatabase.getDatabase(application).cityDao()
        repository = WeatherRepository(cityDao)
//
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(city: CityFound) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(city)
    }

    fun getWeatherById(id: Int): String {
        return repository.getWeatherById(id)
    }

    fun findCity(cityName: String?) {

        coroutineScope.launch {
            cities.postValue(repository.findCities(cityName).await().list)
        }
    }
}


