package weatherapp.emptymindgames.weather_app

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import weatherapp.emptymindgames.weather_app.data.CityFound


class ChooseActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var weatherViewModel: WeatherViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose)
        weatherViewModel = ViewModelProvider(this).get(WeatherViewModel::class.java)

        val s: String? = intent.getStringExtra("cityname")

        var listCities: List<CityFound>?  = null
        viewManager = LinearLayoutManager(this)
        viewAdapter = MyAdapter(listCities)
        weatherViewModel.findCity(s)

        weatherViewModel.cities.observe(this, Observer<List<CityFound>> { newcities ->
            listCities = newcities
        })

        recyclerView = findViewById<RecyclerView>(R.id.recyclerview).apply {
            setHasFixedSize(true)

            layoutManager = viewManager

            adapter = viewAdapter
        }
        weatherViewModel.findCity(s)


    }
}


