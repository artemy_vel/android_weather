package weatherapp.emptymindgames.weather_app

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import weatherapp.emptymindgames.weather_app.data.CityDao
import weatherapp.emptymindgames.weather_app.data.CityFound
import weatherapp.emptymindgames.weather_app.network.CityApi
import weatherapp.emptymindgames.weather_app.network.FindCityResponse


class WeatherRepository(private val cityDao: CityDao) {

    // The internal MutableLiveData String that stores the most recent response


    private var serverJob = Job()



    private val coroutineScope = CoroutineScope(serverJob + Dispatchers.Main)

    suspend fun insert(city: CityFound) {
        cityDao.insertAll(city)
    }

    fun getWeatherById(id: Int): String {
        val url: String =
            "http://api.openweathermap.org/data/2.5/weather?id=" + id + "&units=metric&appid=e2c87117b2042985502a691669a39e51"

        coroutineScope.launch {
            val cityWeather: Deferred<String> = CityApi.retrofitService.getWeatherById(url)
            try {
                var all = cityWeather.await()
            } catch (e: java.lang.Exception) {

            }
        }


        return ""
    }


   fun findCities(cityName: String?):Deferred<FindCityResponse> {
        var url: String =
            "https://api.openweathermap.org/data/2.5/find?q=" + cityName + "&appid=e2c87117b2042985502a691669a39e51"
        return  CityApi.retrofitService.findCity(url)
    }


}