package weatherapp.emptymindgames.weather_app.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface CityDao {
    @Query("SELECT * FROM CityFound")
    fun getAllCities(): LiveData<List<CityFound>>

    @Query("SELECT * FROM CityFound WHERE id IN (:userIds)")
    fun loadAllByIds(userIds: IntArray): List<CityFound>

//    @Query("SELECT * FROM City WHERE first_name LIKE :first AND " +
//            "last_name LIKE :last LIMIT 1")
//    fun findByName(first: String, last: String): City

    @Insert
    fun insertAll( cities: CityFound)

    @Delete
    fun delete(city: CityFound)
}