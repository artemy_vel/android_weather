package weatherapp.emptymindgames.weather_app.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import weatherapp.emptymindgames.weather_app.network.Sys

@Entity
data class CityFound(

    @PrimaryKey var id: Int,
    @ColumnInfo var name: String? = null,
    @Ignore val sys: Sys? = null


) {constructor() : this(0,"",Sys(""))}