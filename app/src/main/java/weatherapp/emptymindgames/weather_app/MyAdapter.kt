package weatherapp.emptymindgames.weather_app

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import weatherapp.emptymindgames.weather_app.data.CityFound

class MyAdapter(private val listCities: List<CityFound>?) :
    RecyclerView.Adapter<MyAdapter.MyViewHolder>() {


    class MyViewHolder(itemView: ConstraintLayout) : RecyclerView.ViewHolder(itemView) {

        private var cityName: TextView? = null
        private var countryName: TextView? = null

        init {
            cityName = itemView.findViewById(R.id.name_city_choose)
            countryName = itemView.findViewById(R.id.country_choose)
        }

        fun bind(city: CityFound?) {
            cityName?.text = city?.name
            countryName?.text = city?.sys.toString()
        }
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val city: CityFound? = listCities?.get(position)
        holder.bind(city)
    }


    override fun getItemCount(): Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val item = LayoutInflater.from(parent.context)
            .inflate(R.layout.one_city_in_list, parent, false) as ConstraintLayout

        return MyViewHolder(item)
    }
}