package weatherapp.emptymindgames.weather_app

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import weatherapp.emptymindgames.weather_app.databinding.ActivityMainBinding
import weatherapp.emptymindgames.weather_app.network.Sys
import weatherapp.emptymindgames.weather_app.data.CityFound
import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream
private lateinit var weatherViewModel: WeatherViewModel


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        weatherViewModel = ViewModelProvider(this).get(WeatherViewModel::class.java)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the options menu from XML
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)

        // Get the SearchView and set the searchable configuration
        val manager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchItem = menu.findItem(R.id.search)
        val searchView = searchItem?.actionView as SearchView

        searchView.setSearchableInfo(manager.getSearchableInfo(componentName))

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                searchView.clearFocus()
                searchView.setQuery("", false)
                searchItem.collapseActionView()
//                weatherViewModel.findCity(query)
                val intent: Intent = Intent(applicationContext, ChooseActivity::class.java).apply {

                    putExtra("cityname",query)
                }
                startActivity(intent)

                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {

                return false
            }

        })




        return true
    }






}
